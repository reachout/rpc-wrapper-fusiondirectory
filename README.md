# FusionDirectory Service Component

XWiki component to manage data in FusionDirectory

## Summary

In order to use this component, you should have a file 
/etc/ow2/wrappers/wrappers.conf containing:

```KEYFILE_LOCATION=path_to_file```

where path_to_file is the file which was used to encrypt the administrator password
using the companion crypt component.

In order to use the component, you should call the init() method with the following parameters:
1. FD_URL : The URL where FusionDirectory is accessible.
1. LDAP_ADMIN : The LDAP administrator login
1. LDAP_ADMINPASSWORD : The LDAP administrator encrypted password
1. LDAP_BASE_RDN : The LDAP base RDN for your LDAP installation (e.g. "dc=ow2,dc=org")
1. LDAP_USER_RDN : The LDAP RDN for users, typically "ou=people"

## Documentation

A generated Javadoc can be found [here](https://reachout.ow2.io/rpc-wrapper-fusiondirectory/). 

### Installation

Run `mvn package` in the root of the project folder. This will build a `pc-wrapper-fusiondirectory-x.x.xx-SNAPSHOT.jar` 
file in `target` ready for usage as an XWiki ScriptService.

### Deployment

Deployment requires authorization. You need to put your valid OW2 credentials into Maven's `settings.xml` file like here:

```
<settings>
  <servers>
    <server>
      <id>ow2.snapshot</id>
      <username>USERNAME</username>
      <password>PASS</password>
    </server>
  </servers>
</settings>
```

There are two locations where a settings.xml file may live:

- The Maven install: `${maven.home}/conf/settings.xml`
- A user’s install: `${user.home}/.m2/settings.xml`

For more information, we refer to the [settings reference](https://maven.apache.org/settings.html).

Once done, the deployment process can be started. 
After updating the product version in `pom.xml`, run `mvn deploy`. 
This will deploy the binary to the XWiki repository specified 
inside `pom.xml`. 

Once done, head to the XWiki admin page, 
go to `'Extensions'->'Advanced search'`. 
Search for the id `org.ow2:rpc-wrapper-fusiondirectory` with the current product 
version specified in `pom.xml`. 
Remove this extension and install the new deployed version. 

**Important:** do not attempt to install the new version by 
updating, this will somehow break XWiki. 
Please reinstall the extension by removing and installing.

## Usage (velocity)

This section covers the commonly used API methods. 
Please visit the full [documentation](https://reachout.ow2.io/rpc-wrapper-fusiondirectory/) 
 for more details.

### Initializate the API

Initializing this FusionDirectory Service Component requires calling the init method with:
- The URL of the FusionDirectory server to use
- The login name of a FusionDirectory administrator user
- The password of the FusionDirectory administrator user
- The LDAP base RDN (e.g. "dc=ow2,dc=org")
- The LDAP user RDN (e.g. "ou=people"). User john will be "uid=john,ou=people,dc=ow2,dc=org"

## License

 FusionDirectory Service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 FusionDirectory Service Component is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved