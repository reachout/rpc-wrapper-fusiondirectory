/**
 This file is part of FusionDirectory Service Component.

 FusionDirectory Service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 FusionDirectory Service Component is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTFusionDirectoryICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.fd.internal;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.ow2.fd.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.test.junit5.mockito.ComponentTest;

import java.io.FileInputStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the {@link UserManagement} component.
 */

@ComponentTest
public class UserManagementImplTest {

    private static String admin;
    private static String adminPwd;

    private static UserManagementImpl umi;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementImplTest.class);
    
    @BeforeAll
    static void initAll() throws Exception {
        try {
            FileInputStream is = new FileInputStream("/etc/ow2/wrappers/wrappers.conf");
            Properties propertiesResource = new Properties();
            propertiesResource.load(is);
            String fdurl = (String) propertiesResource.get("FD_URL");
            admin = (String) propertiesResource.get("LDAP_ADMIN");
            adminPwd = (String) propertiesResource.get("LDAP_ADMINPASSWORD");
            umi = new UserManagementImpl(fdurl, admin, adminPwd, "dc=ow2,dc=org", "ou=people");
        } catch (Exception e) {
        	LOGGER.error("Error in test initialization", e);
        	throw e;
        }
    }

    @Test
    public void testLoginInvalidUser() {
        LOGGER.debug("testLoginInvalidUser");
        @SuppressWarnings("rawtypes")
		Map result = umi.login("wronguser", "WpuS9/apZoK+N4qp0Yzv+Pl9/CiInmNc70BiOB0pDOo=");
        LOGGER.debug("Error found: " + result.get("error"));
        LOGGER.debug("Error type found: " + result.get("errorType"));
        assertEquals("ErrorWrapper", result.get("errorType"));
        assertEquals( "java.lang.RuntimeException: Failed: HTTP error code: 401", result.get("error"));
    }

    @Test
    public void testLogin() {
        LOGGER.debug("testLogin");
        boolean success;
        LOGGER.trace("Testing logging for user: {} with password: {}", admin, adminPwd);
        @SuppressWarnings("rawtypes")
		Map result = umi.login(admin, adminPwd);
        String session = (String) result.get("result");
        LOGGER.debug("Error found: " + result.get("error"));
        LOGGER.debug("Error type found: " + result.get("errorType"));
        if (session == null) {
            LOGGER.debug("login failed");
            success = false;
        } else {
            LOGGER.debug("login ok");
            umi.logout(session);
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testGetValidUserEmail() {
        LOGGER.debug("testGetValidUserEmail");
        String email = (String) umi.getEmail("aime").get("result");
        assertEquals( "aime.vareille@wanadoo.fr", email);
    }

    @Test
    public void testGetValidUserEmailDenis() {
        LOGGER.debug("testGetValidUserEmail");
        String email = (String) umi.getEmail("dcaromel").get("result");
        assertEquals( "denis.caromel@sophia.inria.fr", email);
    }

    @Test
    public void testGetValidUserEmailAlex() {
        LOGGER.debug("testGetValidUserEmailAlex");
        @SuppressWarnings("rawtypes")
		Map result = umi.getEmail("alefebvr");
        String email = (String) result.get("result");
        assertEquals("alexandre.lefebvre@laposte.net", email);
    }

    @Test
    public void testGetInvalidUserEmail() {
        LOGGER.debug("testGetInvalidUserEmail");
        String email = (String) umi.getEmail("aimezzz").get("result");
        assertNull(email);
    }

    @Test
    public void testFirstName() {
        LOGGER.debug("testFirstName");
        String firstname = (String) umi.getFirstName("alefebvr").get("result");
        assertEquals("Alexandre", firstname);
    }

    @Test
    public void testGetInvalidFirstName() {
        LOGGER.debug("testGetInvalidFirstName");
        String firstname = (String) umi.getFirstName("aimezzz").get("result");
        assertNull(firstname);
    }

    @Test
    public void testLastName() {
        LOGGER.debug("testLastName");
        String lastname = (String) umi.getLastName("alefebvr").get("result");
        assertEquals( "Lefebvre", lastname);
    }

    @Test
    public void testGetInvalidLastName() {
        LOGGER.debug("testGetInvalidLastName");
        String lastname = (String) umi.getLastName("aimezzz").get("result");
        assertNull(lastname);
    }

    @Test
    public void testGetUserNameAime() {
        LOGGER.debug("testGetUserNameAime");
        String loginname = (String) umi.getUserName("aime.vareille@wanadoo.fr").get("result");
        assertEquals("aime", loginname);
    }

    @Test
    public void testGetUserNameAlex() {
        LOGGER.debug("testGetUserNameAlex");
        String loginname = (String) umi.getUserName("alexandre.lefebvre@laposte.net").get("result");
        assertEquals("alefebvr", loginname);
    }

    @Test
    public void testGetUserNameInvalidEmail() {
        LOGGER.debug("testGetUserNameInvalidEmail");
        String loginname = (String) umi.getUserName("this.is.not@a.valid.email").get("result");
        assertNull(loginname);
    }

    @Test
    public void testGetUserCountryAlex() {
        LOGGER.debug("testGetUserCountryAlex");
        String country = (String) umi.getUserCountry("alefebvr").get("result");
        assertEquals("France", country);
    }

    @Test
    public void testSetUserCountryAlex() {
        LOGGER.debug("testSetUserCountryAlex");
        umi.setUserCountry("alefebvr", "Germany").get("result");
        String countrynew = (String) umi.getUserCountry("alefebvr").get("result");
        umi.setUserCountry("alefebvr", "France").get("result");
        String country = (String) umi.getUserCountry("alefebvr").get("result");
        assertEquals("France", country);
        assertEquals("Germany", countrynew);
    }

    @Test
    public void testSetUserCountryDummy() {
        LOGGER.debug("testSetUserCountryDummy");
        umi.setUserCountry("dummyuserwrapper", "Zimbabwe");
        String countrynew = (String) umi.getUserCountry("dummyuserwrapper").get("result");
        assertEquals("Zimbabwe", countrynew);
    }

    @Test
    public void testListLdaps() {
        LOGGER.debug("testListLdaps");
        JSONObject output = umi.call("listLdaps", new String[]{});
        String result = (String) ((JSONObject) output.get("result")).values().toArray()[0];
        assertEquals( "default", result);
    }

    @Test
    public void testCallLsUser() {
        LOGGER.debug("testCallLsUser");
        String sessionid = (String) umi.login(admin, adminPwd).get("result");
        JSONObject output = umi.call("ls", new String[]{sessionid, "user", null, "uid=alefebvr,ou=people,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);
        String result;
        int numusers;
        if ((output.get("result") instanceof JSONArray)
                && ((JSONArray) output.get("result")).isEmpty()) {
            numusers = 0;
            result = "";
        } else {
            numusers = ((JSONObject) output.get("result")).keySet().toArray().length;
            result = (String) ((JSONObject) output.get("result")).keySet().toArray()[0];
        }
        LOGGER.debug("User present " + numusers + " time(s)");
        assertEquals( "uid=alefebvr,ou=people,dc=ow2,dc=org", result);
    }

    @Test
    public void testCallLsOrg() {
        LOGGER.debug("testCallLsOrg");
        String sessionid = (String) umi.login(admin, adminPwd).get("result");
        JSONObject output = umi.call("ls", new String[]{sessionid,"organization", null, "ou=organizations,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);

        String result = (String) ((JSONObject) output.get("result")).values().toArray()[0];
        assertEquals( "fundingbox", result);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
    public void testCallLsOrgFilter() {
        LOGGER.debug("testCallLsOrgFilter");
        String sessionid = (String) umi.login(admin, adminPwd).get("result");
        HashMap params = new HashMap();
        params.put("manager", "1");
        JSONObject output = umi.call("ls", new Object[]{sessionid,"organization", params, "ou=organizations,dc=ow2,dc=org", "(|(fdCommunityMembershipType=strategic)(fdCommunityMembershipType=corporate))"});
        LOGGER.debug("Output: {}", output);
        Map theorgs = ((JSONObject) output.get("result"));
        LOGGER.debug("Num orgs " + theorgs.size());
        String org = (String) theorgs.keySet().toArray()[0];
        assertEquals( "o=fundingbox,ou=organizations,dc=ow2,dc=org", org);
        String manager = (String) ((Map)theorgs.values().toArray()[0]).get("manager");
        assertEquals( "uid=amottier,ou=people,dc=ow2,dc=org", manager);

    }

    @Test
    public void testWrapperCallLsOrg() {
        LOGGER.debug("testWrapperCallLsOrg");
        JSONObject output = umi.wrapperCall("ls", new String[]{"organization", null, "ou=organizations,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);

        String result = (String) ((JSONObject) output.get("result")).values().toArray()[0];
        assertEquals( "fundingbox", result);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testCallLsOrgArgs() {
        LOGGER.debug("testCallLsOrgArgs");
        String sessionid = (String) umi.login(admin, adminPwd).get("result");
        JSONObject output = umi.call("ls", new String[]{sessionid,"organization", "{\"manager\":\"*\",\"description\":\"*\"}", "ou=organizations,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);

        Map res = (Map) ((JSONObject) output.get("result")).values().toArray()[0];
        String result = (String) ((JSONArray) res.get("manager")).get(0);
        assertEquals( "uid=amottier,ou=people,dc=ow2,dc=org", result);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testWrapperCallLsOrgArgs() {
        LOGGER.debug("testWrapperCallLsOrgArgs");
        JSONObject output = umi.wrapperCall("ls", new String[]{"organization", "{\"manager\":\"*\",\"description\":\"*\"}", "ou=organizations,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);

        Map res = (Map) ((JSONObject) output.get("result")).values().toArray()[0];
        String result = (String) ((JSONArray) res.get("manager")).get(0);
        assertEquals( "uid=amottier,ou=people,dc=ow2,dc=org", result);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testCallLsOrgArgsMap() {
        LOGGER.debug("testCallLsOrgArgsMap");
        String sessionid = (String) umi.login(admin, adminPwd).get("result");

        HashMap params = new HashMap();
        params.put("manager", "*");
        params.put("description", "*");
        JSONObject output = umi.call("ls", new Object[]{sessionid,"organization", params, "ou=organizations,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);

        Map res = (Map) ((JSONObject) output.get("result")).values().toArray()[0];
        String result = (String) ((JSONArray) res.get("manager")).get(0);
        assertEquals( "uid=amottier,ou=people,dc=ow2,dc=org", result);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testWrapperCallLsOrgArgsMap() {
        LOGGER.debug("testWrapperCallLsOrgArgsMap");

        HashMap params = new HashMap();
        params.put("manager", "*");
        params.put("description", "*");
        JSONObject output = umi.wrapperCall("ls", new Object[]{"organization", params, "ou=organizations,dc=ow2,dc=org", null});
        LOGGER.debug("Output: {}", output);

        Map res = (Map) ((JSONObject) output.get("result")).values().toArray()[0];
        String result = (String) ((JSONArray) res.get("manager")).get(0);
        assertEquals( "uid=amottier,ou=people,dc=ow2,dc=org", result);
    }

    @SuppressWarnings("unchecked")
	@Test
    public void testJSONObject() {
        JSONObject jo = new JSONObject();
        jo.put("Hello","Bonjour");
        LOGGER.debug(jo.toString());

        String[] tlt = new String[]{"bonjour","hola"};
        JSONArray ja = new JSONArray();
        ja.addAll(Arrays.asList(tlt));
        LOGGER.debug(ja.toString());

        jo.put("Hello2",ja);
        LOGGER.debug(jo.toString());

        assertEquals("{\"Hello\":\"Bonjour\",\"Hello2\":[\"bonjour\",\"hola\"]}", jo.toString());
    }

    @Test
    public void testListTypes() {
        LOGGER.debug("testListTypes");
        String sessionid = (String) umi.login(admin, adminPwd).get("result");
        JSONObject output = umi.call("listTypes", new String[]{sessionid});
        LOGGER.debug("Output: {}", output);
        String result = (String) ((JSONObject) output.get("result")).values().toArray()[0];
        umi.logout(sessionid);
        assertEquals( "Terminal", result);
    }

    @Test
    public void testGroupMembers() {
        LOGGER.debug("testGroupMembers");
        String[] members = (String[]) umi.getGroupMembers("cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org").get("result");
        assertEquals("mh", members[0]);
    }

    @Test
    public void testGetUserGroups() {
        LOGGER.debug("testGetUserGroups");
        String[] groups = (String[]) umi.getUserGroups("alefebvr").get("result");
        assertEquals("cn=individuals,ou=groups,dc=ow2,dc=org", groups[0]);
    }

    @SuppressWarnings("rawtypes")
    @Test
    @Disabled
    //FIXME: remove created test user at the end of the test and then enable again this test
    public void testCreateUser() {
        LOGGER.debug("testCreateUser");
        Map resultMap = umi.createUser("dummyuserwrapper", "Test", "Wrapper", "dummytestwrapper@yopmail.fr", "password", "password", "Germany");
        Object result = resultMap.get("result");
        assertEquals("uid=dummyuserwrapper,ou=people,dc=ow2,dc=org", result);
    }

    @SuppressWarnings("rawtypes")
    @Test
    @Disabled
    //FIXME: remove created test user at the end of the test and then enable again this test
    public void testCreateUserNoCountry() {
        LOGGER.debug("testCreateUserNoCountry");
        Map resultMap = umi.createUser("dummyuserwrappernc", "Test", "Wrapper", "dummytestwrappernc@yopmail.fr", "password", "password", null);
        Object result = resultMap.get("result");
        assertEquals("uid=dummyuserwrappernc,ou=people,dc=ow2,dc=org", result);
    }
    @Test
    public void testDeleteUser() {
        LOGGER.debug("testDeleteUser");
        Object result = umi.deleteUser("dummyuserwrapper").get("result");
        assertNull(result);
    }

    @SuppressWarnings("rawtypes")
    @Test
    @Disabled
    public void testUnknownDeleteUser() {
        LOGGER.debug("testDeleteUser");
        Map result =  umi.deleteUser("thisisnotauser7");
        Object error = result.get("error");
        LOGGER.debug("Error: " + error);
        Object errorType = result.get("errorType");
        LOGGER.debug("Error type: " + errorType);
        Object theresult = result.get("result");
        LOGGER.debug("Result: " + theresult);
        assertEquals("Could not open dn uid=thisisnotauser7,ou=people,dc=ow2,dc=org", error);
    }

    @Test
    public void testAddUserToGroup() {
        LOGGER.debug("testAddUserToGroup");
        String[] members = (String[]) umi.addUserToGroup("cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org", "dummyuserwrapper").get("result");
        assertEquals("mh", members[0]);
    }

    @Test
    public void testRemoveUserFromGroup() {
        LOGGER.debug("testRemoveUserFromGroup");
        String[] members = (String[]) umi.removeUserFromGroup("cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org", "dummyuserwrapper").get("result");
        assertEquals("mh", members[0]);
    }

    @Test
    public void testResetUserPassword() {
        LOGGER.debug("testResetUserPassword");
        Object result = umi.resetUserPassword("dummyuserwrapper", "newPassword2", "newPassword2").get("result");
        assertEquals("uid=dummyuserwrapper,ou=people,dc=ow2,dc=org", result);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testLockUserInvalid() {
        LOGGER.debug("testLockUser");
        Map callLock = umi.lockUser("alefebvrxxxxx");
        Object error = callLock.get("error");
        LOGGER.debug("Error found: " + error);
        LOGGER.debug("Error type found: " + callLock.get("errorType"));
        assertNotNull(error);
        assertEquals("ErrorFD", callLock.get("errorType"));
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testLockUser() {
        LOGGER.debug("testLockUser");
        umi.createUser("dummyuserwrapper", "Test", "Wrapper", "dummytestwrapper@yopmail.fr", "password", "password", "Japan");
        Map callLock =  umi.lockUser("dummyuserwrapper");
        Object result = callLock.get("result");
        Object error = callLock.get("error");
        assertNull(result);
        assertNull(error);
        Map callLogin = umi.login("dummyuserwrapper", "iWexYeORwAwEBrVypEVKQw==");
        Object loginError = callLogin.get("error");
        LOGGER.debug("loginError: {}", loginError);
        umi.unlockUser("dummyuserwrapper");
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testLs() {
        LOGGER.debug("testLs");
        Map callLs = umi.ls("OGROUP", null, null, "");
        JSONObject result = (JSONObject) callLs.get("result");
        Object error = callLs.get("error");
        assertEquals("prelude-members", result.values().toArray()[0]);
        assertNull(error);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testLsUserByEmail() {
        LOGGER.debug("testLsUserByEmail");
        Map callLs = umi.ls("USER", "mail", null, "(mail=Aime.Vareille@wanadoo.fr)");
        JSONObject result = (JSONObject) callLs.get("result");
        Object error = callLs.get("error");
        assertEquals("uid=aime,ou=people,dc=ow2,dc=org", result.keySet().toArray()[0]);
        assertNull(error);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testLsUserOrg() {
        LOGGER.debug("testLsUserOrg");
        HashMap params = new HashMap();
        params.put("manager", "*");
        params.put("description", "*");
        Map callLs = umi.ls("organization",params,"ou=organizations,dc=ow2,dc=org", "");
        JSONObject result = (JSONObject) callLs.get("result");
        Object error = callLs.get("error");
        assertEquals("o=fundingbox,ou=organizations,dc=ow2,dc=org", result.keySet().toArray()[0]);
        assertNull(error);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testLsUserOrgString() {
        LOGGER.debug("testLsUserOrgString");
        String paramsS = "{\"manager\":\"*\",\"description\":\"*\"}";
        Map callLs = umi.ls("organization",paramsS,"ou=organizations,dc=ow2,dc=org", "");
        JSONObject result = (JSONObject) callLs.get("result");
        Object error = callLs.get("error");
        assertEquals("o=edifixio,ou=organizations,dc=ow2,dc=org", result.keySet().toArray()[1]);
        assertNull(error);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testLsUserOrgStringError() {
        LOGGER.debug("testLsUserOrgStringError");
        String paramsS = "{\"manager\":\"*\",\"description\"\"*\"}";
        Map callLs = umi.ls("organization",paramsS,"ou=organizations,dc=ow2,dc=org", "");
        JSONObject result = (JSONObject) callLs.get("result");
        Object error = callLs.get("error");
        Object errorType = callLs.get("errorType");
        assertNull(result);
        assertEquals("Unexpected token END OF FILE at position 0.", error);
        assertEquals("ErrorFD", errorType);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testGetFields() {
        LOGGER.debug("testGetFields");
        Map output = umi.getFields("user","uid=alefebvr,ou=people,dc=ow2,dc=org", "userRoles");
        LOGGER.debug("Output: {}", output);
        //['sections']['groups']['attrs']['groupsMembership']['value']
        Map thesection = ((Map)((Map)output.get("result")).get("sections"));
        Map groups = ((Map)thesection.get("groups"));
        Map attrs = ((Map)groups.get("attrs"));
        Map membership =  ((Map)attrs.get("groupsMembership"));
        Object thegroups = membership.get("value");
        Object result = ((JSONArray) thegroups).toArray()[0];
        assertEquals( "cn=individuals,ou=groups,dc=ow2,dc=org", result);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void testUserOrgManager() {
        LOGGER.debug("testUserOrgManager");
        Map output = umi.isUserNotInGroupAndOrgManager("dcaromel", "cn=individuals,ou=groups,dc=ow2,dc=org",
                "ou=organizations", "(|(fdCommunityMembershipType=strategic)(fdCommunityMembershipType=corporate))");
        Object isUser = ((Map) output.get("result")).get("userExists");
        assertEquals(true, isUser);
        Object isIndividual = ((Map) output.get("result")).get("isGroupMember");
        assertEquals(true, isIndividual);
    }

    @SuppressWarnings("rawtypes")
	@Test
    public void testUserOrgManagerActiveeon() {
        LOGGER.debug("testUserOrgManagerActiveeon");
        Map output = umi.isUserNotInGroupAndOrgManager("activeeon", "cn=individuals,ou=groups,dc=ow2,dc=org",
                "ou=organizations", "(|(fdCommunityMembershipType=strategic)(fdCommunityMembershipType=corporate))");
        Object isUser = ((Map) output.get("result")).get("userExists");
        assertEquals(true, isUser);
        Object isIndividual = ((Map) output.get("result")).get("isGroupMember");
        assertEquals(false, isIndividual);
        String org = (String) ((ArrayList) ((Map) output.get("result")).get("orgsManaged")).get(0);
        assertEquals("o=activeeon,ou=organizations,dc=ow2,dc=org", org);
    }

    @SuppressWarnings("rawtypes")
	@Test
    public void testUserOrgManagerActiveeonWrongFilter() {
        LOGGER.debug("testUserOrgManagerActiveeonWrongFilter");
        Map output = umi.isUserNotInGroupAndOrgManager("activeeon", "cn=individuals,ou=groups,dc=ow2,dc=org",
                "ou=organizations", "(|(fdCommunityMembershipType=strategic)(bananas=corporate))");
        Object isUser = ((Map) output.get("result")).get("userExists");
        assertEquals(true, isUser);
        Object isIndividual = ((Map) output.get("result")).get("isGroupMember");
        assertEquals(false, isIndividual);
        assertTrue(((ArrayList) ((Map) output.get("result")).get("orgsManaged")).isEmpty());
    }

    @SuppressWarnings("rawtypes")
	@Test
    public void testUserOrgManagerBonita() {
        LOGGER.debug("testUserOrgManagerBonita");
        Map output = umi.isUserNotInGroupAndOrgManager("bonitasoft", "cn=individuals,ou=groups,dc=ow2,dc=org",
                "ou=organizations", "(|(fdCommunityMembershipType=strategic)(fdCommunityMembershipType=corporate))");
        Object isUser = ((Map) output.get("result")).get("userExists");
        assertEquals(true, isUser);
        Object isIndividual = ((Map) output.get("result")).get("isGroupMember");
        assertEquals(false, isIndividual);
        String org = (String) ((ArrayList) ((Map) output.get("result")).get("orgsManaged")).get(0);
        assertEquals("o=bonitasoft,ou=organizations,dc=ow2,dc=org", org);
    }

    //TODO more tests from https://api.fusiondirectory.org/classfdRPCService.html
}
