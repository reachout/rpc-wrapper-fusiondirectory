/**
 This file is part of FusionDirectory Service Component.

 FusionDirectory Service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 FusionDirectory Service Component is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.fd.internal;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ow2.fd.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.component.annotation.Component;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Singleton;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Implementation of a <tt>FusionDirectory</tt> component.
 */
@Component
@Singleton
public class UserManagementImpl implements UserManagement {

    private String admin;
    private String adminPwd; //the encrypted admin password
    private String ldapBaseRDN; //the LDAP base DN, e.g. "dc=org,dc=ow2"
    private String ldapUserRDN; //the LDAP RDN for users, e.g. "ou=people"

    private String keyFileLocation;
    private Properties propertiesResource;
    private String md5;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementImpl.class);

    private final static String ERRORFD = "ErrorFD";
    private final static String ERRORWRAPPER = "ErrorWrapper";

    private WebResource webResource;

    /**
     * There should be on the filesystem:
     * 1. file /etc/ow2/wrappers/wrappers.conf containing a line KEYFILE_LOCATION=path_to_file
     * 2. file path_to_file should also exist
     */
    public UserManagementImpl() {
        try {
            FileInputStream is = new FileInputStream("/etc/ow2/wrappers/wrappers.conf");
            propertiesResource = new Properties();
            propertiesResource.load(is);
            keyFileLocation = (String) propertiesResource.get("KEYFILE_LOCATION");
            FileInputStream keyfileio = new FileInputStream(keyFileLocation);
            md5 = DigestUtils.md5Hex(keyfileio);
        } catch (IOException e) {
            LOGGER.error("Error while loading configuration", e);
        }
    }

    public UserManagementImpl(String url, String adminUser, String adminUserPwd, String ldapBaseDN, String ldapUserDN) throws Exception {
        FileInputStream is = new FileInputStream("/etc/ow2/wrappers/wrappers.conf");
        propertiesResource = new Properties();
        propertiesResource.load(is);
        keyFileLocation = (String) propertiesResource.get("KEYFILE_LOCATION");
        FileInputStream keyfileio = new FileInputStream(keyFileLocation);
        md5 = DigestUtils.md5Hex(keyfileio);
        setup(url);
        admin = adminUser;
        adminPwd = adminUserPwd;
        ldapBaseRDN = ldapBaseDN;
        ldapUserRDN = ldapUserDN;
        LOGGER.debug("UserManagementImpl instantiated with URL: {}, user: {}, LDAP base DN: {}, LDAP user DN: {}", url, adminUser, ldapBaseDN, ldapUserDN);
    }

    /**
     * Decrypt a String
     * @param input : the String to be decrypted
     * @return the decrypted String
     */
    private String decrypt(String input) {
        try {
            byte[] decodedKey = Base64.getDecoder().decode(md5);
            Cipher cipher = Cipher.getInstance("AES");
            // rebuild key using SecretKeySpec
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.DECRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(Base64.getDecoder().decode(input));
            return new String(cipherText);
        } catch (Exception e) {
        	LOGGER.error("Error while decrypting", e);
            throw new RuntimeException(
                    "Error occurred while decrypting data", e);
        }
    }

    /**
     * Setup HTTPBuilder with Fusion Directory URL
     * @param url : URL of FD server
     */
    private void setup(String url) {
    	LOGGER.trace("Setting up the client using URL: {}", url);
        Client client = Client.create();
        LOGGER.trace("Client created: {}", client);
        webResource = client.resource(url);
        LOGGER.trace("WebTarget for: {} created", url);
    }

    /**
     * Builds the FusionDirectory call from the parameters
     * @param params an array of strings containing the parameters
     * @return the FusionDirectory input string
     */
    private String buildCall(String method, Object[] params) {
        StringBuilder input = new StringBuilder("{\"id\":\"hello\",\"jsonrpc\":\"2.0\",\"method\":\"");
        input.append(method).append("\",\"params\":[");
        for( int i = 0; i < params.length; i++) {
            //get String value for params[i]
            String paramString = getJSONStringValue(params[i]);
            if (i == params.length-1) { //last one, no comma
                    input.append(paramString);
            } else { //more to come, add comma
                input.append(paramString).append(",");
            }
        }
        input.append("]}");
        return input.toString();
    }

    /**
     * Returns the JSON String value of an object.
     * The object can be a String, an array of String, or a Map with String as values.
     * @param param the Object to be converted
     * @return the JSON String value of the Object
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public String getJSONStringValue(Object param) {
        if (param instanceof String) {
            return stringIfNotNull((String)param);
        } else if (param instanceof String[]) {
            JSONArray ja = new JSONArray();
            ja.addAll(Arrays.asList((String[])param));
            return ja.toString();
        } else if (param instanceof Map) {
            JSONObject jo = new JSONObject();
            jo.putAll((Map) param);
            return jo.toString();
        }
        return null;
    }

    /**
     * Builds a String being "null" if param is null or "\"param\"" if param is not null
     * @param param: the String parameter
     * @return the built String
     */
    private String stringIfNotNull(String param) {
        if (param == null) {
            return "null";
        } else {
            if (param.startsWith("{")) {
                return param;
            } else {

            return "\"" + param + "\"";
            }
        }
    }

    /**
     * Log into FusionDirectory
     * @param user : the user login name
     * @param pwd : the user password
     * @return a Map containing the session id associated to the login process in case of success
     */
    @SuppressWarnings("rawtypes")
	@Override
    public Map login(String user, String pwd) {
        LOGGER.debug("Trying to authenticate user: {}", user);
        JSONObject jobj = call("login", new String[] {null, user, decrypt(pwd)});
        LOGGER.debug("New FusionDirectory wrapper session (login)");
        return jobj;
    }

    /**
     * Log out from FusionDirectory
     * @param sessionid: the session id used for logging in
     */
    @Override
    public void logout(String sessionid) {
        try {
            JSONObject jobj = call("logout", new String[] {sessionid});

            if(jobj.get("result").equals(Boolean.FALSE)) {
                throw new Exception("Logout failed ");
            }
        } catch (Exception e) {
            LOGGER.error("Exception in FusionDirectory wrapper", e);
        }
    }

    /**
     * Gets the email of a user identified by a login name
     * @param userId: the user login name
     * @return A Map containing error and errorType, and result with the user e-mail address.
     * If no e-mail address was found for the given login name, the result is null.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getEmail(String userId) {
        LOGGER.debug("Trying to get email address for user id: {}", userId);
        String email;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            LOGGER.debug("Authentication performed in getEmail is successful");
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new String[]{sessionid, "USER", "mail", null, "(uid=" + userId + ")"});
            LOGGER.debug("Answer received after calling \"ls USER\" for user id: {} is: {}", userId, jobj);
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty()) {
                LOGGER.debug("Result of \"ls USER\" is a JSONArray and \"result\" attribute is empty");
                email = null;
            } else {
                email = (String) ((JSONObject) jobj.get("result")).values().toArray()[0];
            }
            logout(sessionid);
            result.put("result", email);
            result.put("error", null);
            result.put("errorType", null);
        } else {
            LOGGER.debug("Authentication performed in getEmail failed");
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Gets the first name of a user identified by a login name
     * @param userId : the user login name
     * @return A Map containing error and errorType, and result with the user first name.
     * If no first name was found for the given login name, the result is null.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getFirstName(String userId) {
        HashMap result = new HashMap();
        String firstname;
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new String[]{sessionid, "USER", "givenName", null, "(uid=" + userId + ")"});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty())
                firstname = null;
            else {
                firstname = (String) ((JSONObject) jobj.get("result")).values().toArray()[0];
            }
            logout(sessionid);
            result.put("result", firstname);
            result.put("error", null);
            result.put("errorType", null);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }
    /**
     * Gets the last name of a user identified by a login name
     * @param userId: the user login name
     * @return A Map containing error and errorType, and result with the user last name.
     * If no last name was found for the given login name, the result is null.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getLastName(String userId) {
        String lastname;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new String[]{sessionid, "USER", "sn", null, "(uid=" + userId + ")"});

            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty())
                lastname = null;
            else {
                lastname = (String) ((JSONObject) jobj.get("result")).values().toArray()[0];
            }
            logout(sessionid);
            result.put("result", lastname);
            result.put("error", null);
            result.put("errorType", null);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Gets the country of a user identified by a login name
     * @param userId: the user login name
     * @return A Map containing error and errorType, and result with the user country.
     * If no country was found for the given login name, the result is null.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getUserCountry(String userId) {
        LOGGER.debug("Trying to get country for user id: {}", userId);
        String country = null;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            LOGGER.debug("Authentication performed in getUserCountry is successful");
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new String[]{sessionid, "USER", "co", null, "(uid=" + userId + ")"});
            LOGGER.debug("Answer received after calling \"ls USER\" to get co (country) attribute for user id: {} is: {}", userId, jobj);
            if ((jobj.get("result") instanceof JSONObject)
                    && !((JSONObject) jobj.get("result")).isEmpty()) {
                country = (String) ((JSONObject) jobj.get("result")).values().toArray()[0];
            }
            logout(sessionid);
            result.put("result", country);
            result.put("error", null);
            result.put("errorType", null);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Sets the country of a user identified by a login name
     * @param userId: the user login name
     * @param country: the country to be set for this user
     * @return A Map containing error and errorType, and result with the user id.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map setUserCountry(String userId, String country) {
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("setFields", new String[]{sessionid, "USER", "uid="+userId+","+ldapUserRDN+","+ ldapBaseRDN, "{\"personalInfo\":{\"co\":\""+country+"\"}}"});

            if ((jobj.get("error") == null)) { //no error
                logout(sessionid);
                result.put("result", jobj.get("result"));
                result.put("error", null);
                result.put("errorType", null);
            } else {
                logout(sessionid);
                result.put("result", null);
                result.put("error", jobj.get("error"));
                result.put("errorType", ERRORFD);
            }
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }


    /**
     * Gets the login name of a user identified by an email address
     * @param email: the user e-mail
     * @return A Map containing error and errorType, and result with the username.
     * If no username was found for the given email, the result is null.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getUserName(String email) {
        String username;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new String[]{sessionid, "USER", "mail", null, "(mail=" + email + ")"});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty())
                username = null;
            else {
                String dn = (String) ((JSONObject) jobj.get("result")).keySet().toArray()[0];
                //should be in the form of uid=loginname,ou=people,dc=ow2,dc=org
                username = dn.substring(4, dn.indexOf(","));
            }
            logout(sessionid);
            result.put("result", username);
            result.put("error", null);
            result.put("errorType", null);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Gets the list of members of a group
     * @param group the full group name,
     *             e.g. "cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org"
     * @return a map of user id (members) of that group
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getGroupMembers(String group) {
        String[] userids;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("getFields", new String[]{sessionid, "OGROUP", group, null});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty())
                userids = null;
            else {
                JSONObject output = (JSONObject) jobj.get("result");
                JSONObject sections = (JSONObject) output.get("sections");
                JSONObject members = (JSONObject) sections.get("members");
                JSONObject memberattrs = (JSONObject) members.get("attrs");
                JSONObject member = (JSONObject) memberattrs.get("member");
                JSONArray value = (JSONArray) member.get("value");
                Iterator it = value.iterator();
                String[] theresult = new String[value.size()];
                int i = 0;
                String aMember;
                while (it.hasNext()) {
                    aMember = (String) it.next();
                    theresult[i] = aMember.substring(4, aMember.indexOf(","));
                    i++;
                }
                userids = theresult;
            }
            logout(sessionid);
            result.put("result", userids);
            result.put("error", null);
            result.put("errorType", null);
        }
        else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Adds a user to a group
     * @param group the full group name,
     *             e.g. "cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org"
     * @param user the member to add to the group
     * @return A Map containing error and errorType, and result with an array of usernames in that group
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map addUserToGroup(String group, String user) {
        String[] userids;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("getFields", new String[]{sessionid, "OGROUP", group, null});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty())
                result = null;
            else {
                JSONObject output = (JSONObject) jobj.get("result");
                JSONObject sections = (JSONObject) output.get("sections");
                JSONObject members = (JSONObject) sections.get("members");
                JSONObject memberattrs = (JSONObject) members.get("attrs");
                JSONObject member = (JSONObject) memberattrs.get("member");
                JSONArray value = (JSONArray) member.get("value");
                //add the user to the group
                value.add("uid=" + user + ","+ldapUserRDN+"," + ldapBaseRDN);

                //compute the list of members
                Iterator it = value.iterator();
                String[] theresult = new String[value.size()];
                int i = 0;
                String aMember;
                while (it.hasNext()) {
                    aMember = (String) it.next();
                    theresult[i] = aMember.substring(4, aMember.indexOf(","));
                    i++;
                }
                userids = theresult;
                String newusers = buildGroupUsersArray(userids);
                newusers = "{\"ogroup\":{\"member\":" + newusers + "}}";
                //put the members into FD
                call("setFields", new String[]{sessionid, "OGROUP", group, newusers});

                result.put("result", userids);
                result.put("error", null);
                result.put("errorType", null);
            }
            logout(sessionid);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Removes a user from a group
     * @param group the full group name,
     *             e.g. "cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org"
     * @param user the member to remove from the group
     * @return A Map containing error and errorType, and result with an array of usernames in that group
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map removeUserFromGroup(String group, String user) {
        String[] userids;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("getFields", new String[]{sessionid, "OGROUP", group, null});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty())
                result = null;
            else {
                JSONObject output = (JSONObject) jobj.get("result");
                JSONObject sections = (JSONObject) output.get("sections");
                JSONObject members = (JSONObject) sections.get("members");
                JSONObject memberattrs = (JSONObject) members.get("attrs");
                JSONObject member = (JSONObject) memberattrs.get("member");
                JSONArray value = (JSONArray) member.get("value");
                //add the user to the group
                value.remove("uid=" + user + ","+ldapUserRDN+"," + ldapBaseRDN);

                //compute the list of members
                Iterator it = value.iterator();
                String[] theresult = new String[value.size()];
                int i = 0;
                String aMember;
                while (it.hasNext()) {
                    aMember = (String) it.next();
                    theresult[i] = aMember.substring(4, aMember.indexOf(","));
                    i++;
                }
                userids = theresult;
                String newusers = buildGroupUsersArray(userids);
                newusers = "{\"ogroup\":{\"member\":" + newusers + "}}";
                //put the members into FD
                call("setFields", new String[]{sessionid, "OGROUP", group, newusers});

                result.put("result", userids);
                result.put("error", null);
                result.put("errorType", null);
            }
            logout(sessionid);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    private String buildGroupUsersArray(String[] users) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for( int i = 0; i < users.length; i++) {
            if (i == users.length-1) { //last one, no comma
                sb.append("\"uid=").append(users[i]).append(",").append(ldapUserRDN).append(",").append(ldapBaseRDN).append("\"");
            } else {
                sb.append("\"uid=").append(users[i]).append(",").append(ldapUserRDN).append(",").append(ldapBaseRDN).append("\",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Get the groups to which a user belongs
     * @param user the user login name
     * @return A Map containing error and errorType, and result with an array of groups to which the user belongs
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map getUserGroups(String user) {
        String[] thegroups;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("getfields", new String[]{sessionid, "USER", "uid="+user+","+ldapUserRDN+","+ ldapBaseRDN, "userRoles"});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty()) {
                result = null;
            } else {
                JSONObject output = (JSONObject) jobj.get("result");
                JSONObject sections = (JSONObject) output.get("sections");
                JSONObject groups = (JSONObject) sections.get("groups");
                JSONObject groupattrs = (JSONObject) groups.get("attrs");
                JSONObject groupmembership = (JSONObject) groupattrs.get("groupsMembership");
                JSONArray value = (JSONArray) groupmembership.get("value");
                //compute the list of members
                Iterator it = value.iterator();
                String[] theresult = new String[value.size()];
                int i = 0;
                String aMember;
                while (it.hasNext()) {
                    aMember = (String) it.next();
                    theresult[i] = aMember;
                    i++;
                }
                thegroups = theresult;

                result.put("result", thegroups);
                result.put("error", null);
                result.put("errorType", null);
            }
            logout(sessionid);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Creates a new user
     * @param loginname: the user login name
     * @param first: the user first name
     * @param last: the user last name
     * @param email: the user email
     * @param password: the user password
     * @param password2: the user confirmed password
     * @param country: the user country
     * @return a map containing the user creation result
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map createUser(String loginname, String first, String last, String email, String password, String password2, String country) {
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");

            JSONObject jobj = call("setFields",
                    new String[]{sessionid, "user", null,
                            "{\"user\":{\"base\":\""+ ldapBaseRDN +"\",\"givenName\":\"" + first
                                    + "\",\"sn\":\"" + last + "\",\"uid\":\"" + loginname
                                    + "\",\"userPassword\":[\"\",\"" + password + "\",\"" + password2
                                    + "\",\"\",false]},\"mailAccount\":{\"mail\":\"" + email + "\"}"
                                    + ",\"personalInfo\":{\"co\":\"" + country + "\"}}"});
            Object jobjResult = jobj.get("result");
            if (jobjResult instanceof String) { //this is the userid created
                result.put("result", jobjResult);
                result.put("error", null);
                result.put("errorType", null);
            } else { //something went wrong
                ArrayList error = (ArrayList) ((Map) jobjResult).get("errors");
                result.put("result", null);
                result.put("error", error);
                result.put("errorType", ERRORFD);
            }
            logout(sessionid);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Deletes a new user
     * @param loginname: the user login name
     * @return A Map containing error and errorType, and result with the user deletion result
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map deleteUser(String loginname) {
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("delete",
                    new String[]{sessionid, "user", "uid="+loginname+","+ldapUserRDN+","+ ldapBaseRDN});
            LOGGER.trace("Result of deleting user: {}", jobj.toString());
            Object jobjResult = jobj.get("result");
            if (jobjResult != null) { //there was an error
                ArrayList error = (ArrayList) ((Map) jobjResult).get("errors");
                result.put("result", null);
                result.put("error", error);
                result.put("errorType", ERRORFD);
            } else { //OK went fine
                result.put("result", null);
                result.put("error", null);
                result.put("errorType", null);
            }
            logout(sessionid);
        } else {
            // Login failed
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Resets the password of a user
     * @param loginname: the user login name for which to reset the password
     * @param password: the user new password
     * @param password2: the user confirmed new password
     * @return A Map containing error and errorType, and result with the password reset result
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map resetUserPassword(String loginname, String password, String password2) {
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");

            JSONObject jobj = call("setFields",
                    new String[]{sessionid, "user", "uid="+loginname+","+ldapUserRDN+","+ ldapBaseRDN,
                            "{\"user\":{\"userPassword\":[\"\",\"" + password + "\",\"" + password2
                                    + "\",\"\",false]}}"});
            Object jobjResult = jobj.get("result");
            if (jobjResult instanceof String) { //this is the userid created
                result.put("result", jobjResult);
                result.put("error", null);
                result.put("errorType", null);
            } else { //something went wrong
                ArrayList error = (ArrayList) ((Map) jobjResult).get("errors");
                result.put("result", null);
                result.put("error", error);
                result.put("errorType", ERRORFD);
            }
            logout(sessionid);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Locks a user in the directory
     * @param loginname: the user login name to lock
     * @return A Map containing error and errorType, and result of locking the user
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map lockUser(String loginname) {
        String error = null, errorType = null;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("lockuser", new String[]{sessionid, "uid="+loginname+","+ldapUserRDN+","+ ldapBaseRDN, "lock"});
            if (jobj.get("result") != null) {
                //there is a result which is an error, get the error
                error = ((JSONObject) jobj.get("result")).values().toString();
                errorType = ERRORFD;
            }
            logout(sessionid);
            result.put("result", null);
            result.put("error", error);
            result.put("errorType", errorType);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Unlocks a user in the directory
     * @param loginname: the user login name to unlock
     * @return A Map containing error and errorType, and result of unlocking the user
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map unlockUser(String loginname) {
        String error = null, errorType = null;
        HashMap result = new HashMap();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("lockuser", new String[]{sessionid, "uid="+loginname+","+ldapUserRDN+","+ ldapBaseRDN, "unlock"});
            if (jobj.get("result") != null) {
                //there is a result which is an error, get the error
                error = ((JSONObject) jobj.get("result")).values().toString();
                errorType = ERRORFD;
            }
            logout(sessionid);
            result.put("result", null);
            result.put("error", error);
            result.put("errorType", errorType);
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Calls the FusionDirectory "ls" method without having to log in first.
     * Parameters are, according to <a href="https://api.fusiondirectory.org/classfdRPCService.html#a8398ebf167739c4b6e654daec180a5dd">FusionDirectory documentation</a>
     * @param type the objectType to list
     * @param attrs The attributes to fetch. If this is a single value, the resulting associative array
     *             will have for each dn the value of this attribute.
     *             If this is an array, the keys must be the wanted attributes,
     *             and the values can be either 1, '*' or 'raw'
     * @param ou: the LDAP branch to search in, base will be used if it is NULL
     * @param filter: an additional filter to use in the LDAP search.
     * @return A Map containing error and errorType, and result.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map ls(String type, Object attrs, String ou, String filter) {
        HashMap result = new HashMap();
        Object theResult;
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new Object[] {sessionid, type, attrs, ou, filter});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty()) {
                theResult = null;
            }
            else {
                theResult = jobj.get("result");
            }
            logout(sessionid);
            result.put("result", theResult);
            result.put("error", jobj.get("error"));
            result.put("errorType", jobj.get("errorType"));
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Generic call method to the FusionDirectory JSON API without having to log in first.
     * Note that, for several methods, login must have been performed previously and
     * a session id provided as the first parameter.
     * See the FusionDirectory API for more details.
     * <a href="https://api.fusiondirectory.org/classfdRPCService.html">classfdRPCService</a>
     * @param method: the method to call
     * @param params: an array of Objects containing the parameters
     *              Supported type for Objects are String, String[] or Map with String values.
     * @return a JSONObject containing the parsed output
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public JSONObject wrapperCall(String method, Object[] params) {
        JSONObject result = new JSONObject();
        Object theResult;
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            //add sessionid in front of params
            Object[] newparams = new Object[params.length+1];
            newparams[0]=sessionid;
            System.arraycopy(params, 0, newparams, 1, params.length);
            JSONObject jobj = call(method, newparams);
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty()) {
                theResult = null;
            }
            else {
                theResult = jobj.get("result");
            }
            logout(sessionid);
            result.put("result", theResult);
            result.put("error", jobj.get("error"));
            result.put("errorType", jobj.get("errorType"));
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * Calls the FusionDirectory "getfields" method without having to log in first.
     * Parameters are, according to  <a href="https://api.fusiondirectory.org/classfdRPCService.html#a38cdd9ec1c53f293dfddfa7dcc263d0d">FusionDirectory documentation</a>
     * @param type: the object type
     * @param objectDN: the object to load value from
     * @param tab: the tab to return
     * @return A Map containing error and errorType, and result.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map getFields(String type, String objectDN, String tab) {
        HashMap result = new HashMap();
        Object theResult;
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("getfields", new Object[] {sessionid, type, objectDN, tab});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty()) {
                theResult = null;
            }
            else {
                theResult = jobj.get("result");
            }
            logout(sessionid);
            result.put("result", theResult);
            result.put("error", jobj.get("error"));
            result.put("errorType", jobj.get("errorType"));
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }

    /**
     * From a given userId, groupId and filter, this method:
     * 1. checks whether the user exists
     * 2. whether the user is a member of the input group
     * 3. if the user is NOT a member of the group, get the list of organizations with the filter for
     * which the user is a manager.
     * @param userId the userId to check
     * @param groupId the fully qualified DN for the group to which the user should not belong
     * @param orgRDN the organization DN (e.g. ou=organizations)
     * @param filter a filter to apply to the organizations
     * @return A Map containing error and errorType, and result.
     * result is a map with 3 entries: userExists (boolean value), isGroupMember (boolean value),
     * orgsManaged (ArrayList with the managed organizations).
     *
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map isUserNotInGroupAndOrgManager(String userId, String groupId, String orgRDN, String filter) {
        HashMap result = new HashMap();
        HashMap theResult = new HashMap();
        boolean isGroupMember;
        ArrayList<String> orgsManaged = new ArrayList<>();
        Map login = login(admin, adminPwd);
        if (login.get("error") == null) { //login was ok
            String sessionid = (String) login.get("result");
            JSONObject jobj = call("ls", new Object[] {sessionid, "user", null, "uid="+userId+","+ldapUserRDN+","+ldapBaseRDN});
            if ((jobj.get("result") instanceof JSONArray)
                    && ((JSONArray) jobj.get("result")).isEmpty()) { //user does not exist
                theResult.put("userExists", false);
                theResult.put("isGroupMember", false);
                theResult.put("orgsManaged", orgsManaged);
            }
            else { //user exists
                theResult.put("userExists", true);
                String fullUserId = (String) ((JSONObject) jobj.get("result")).keySet().toArray()[0];
                //finds all groups
                JSONObject jgroups = call("getfields", new Object[] {sessionid, "user", fullUserId, "userRoles"});
                Map thesection = ((Map)((Map)jgroups.get("result")).get("sections"));
                Map groups = ((Map)thesection.get("groups"));
                Map attrs = ((Map)groups.get("attrs"));
                Map membership =  ((Map)attrs.get("groupsMembership"));
                JSONArray thegroups = (JSONArray) membership.get("value");
                //check if user is in group
                isGroupMember = thegroups.contains(groupId);
                theResult.put("isGroupMember", isGroupMember);
                if (!isGroupMember) {
                    //check if the user is a manager of organisations with the filter
                    HashMap manParam = new HashMap();
                    //should use "1"?
                    manParam.put("manager", "*");
                    JSONObject jorgs = call("ls", new Object[] {sessionid, "organization", manParam, orgRDN+","+ldapBaseRDN, filter});
                    Map<String,Map> orgAndManager = (JSONObject) jorgs.get("result");
                    if (orgAndManager != null) {
                        for (Map.Entry<String, Map> entry : orgAndManager.entrySet()) {
                            if (((JSONArray)entry.getValue().get("manager")).contains(fullUserId)) {
                                //user is manager of the org
                                orgsManaged.add(entry.getKey());
                            }
                        }
                    }
                }
                theResult.put("orgsManaged", orgsManaged);
            }
            logout(sessionid);
            result.put("result", theResult);
            result.put("error", jobj.get("error"));
            result.put("errorType", jobj.get("errorType"));
        } else {
            result.put("result", null);
            result.put("error", login.get("error"));
            result.put("errorType", ERRORWRAPPER);
        }
        return result;
    }


    /**
     * Generic call method to the FusionDirectory JSON API.
     * Note that, for several methods, login must have been performed previously and
     * a session id provided as the first parameter.
     * See the FusionDirectory API for more details.
     * <a href="https://api.fusiondirectory.org/classfdRPCService.html">classfdRPCService</a>
     * @param method: the method to call
     * @param params: an array of Objects containing the parameters
     *              Supported type for Objects are String, String[] or Map with String values.
     * @return a JSONObject containing the parsed output
     */
    @SuppressWarnings("unchecked")
	@Override
    public JSONObject call(String method, Object[] params) {
        JSONObject result;
        try {
            LOGGER.trace("Building JSON for calling method: {} with params: {}", method, params);
            String input = buildCall(method, params);
            LOGGER.trace("Preparing a JSON request");
            WebResource.Builder request = webResource.type("application/json");
            LOGGER.trace("Sending request to FusionDirectory with inputs: {} using webResource", input);
			ClientResponse response = request.post(ClientResponse.class, input);
            LOGGER.trace("Response received: {}", response);

            int responseStatus = response.getStatus();
            if (responseStatus != 200) {
                LOGGER.error("Unexpected FusionDirectory response HTTP status code: {} response object: {}", responseStatus, response);
                throw new RuntimeException("Failed: HTTP error code: "
                        + responseStatus);
            }
            String output = response.getEntity(String.class);
            LOGGER.trace("Response to a call to: {} provides output: {}", method, output);
            JSONParser parse = new JSONParser();
            result = (JSONObject) parse.parse(output);
        } catch (Exception e) {
            LOGGER.error("Exception in FusionDirectory wrapper", e);
            result = new JSONObject();
            result.put("error", e.toString());
            if (e instanceof RuntimeException) {
                result.put("errorType", ERRORWRAPPER);
            } else {
                result.put("errorType", ERRORFD);
            }
        }
        return result;
    }
}
