/**
 This file is part of FusionDirectory Service Component.

 FusionDirectory Service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 FusionDirectory Service Component is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.fd.script;

import org.json.simple.JSONObject;
import org.ow2.fd.UserManagement;
import org.ow2.fd.internal.UserManagementImpl;
import org.xwiki.component.annotation.Component;
import org.xwiki.script.service.ScriptService;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * FusionDirectory wrapper API available made available as an XWiki service.
 */
@Component
@Named("fusiondirectory")
@Singleton
public class UserManagementService implements ScriptService
{
    @Inject
    private UserManagement um;

    /**
     * Log into FusionDirectory
     * @param user : the user login name
     * @param pwd : the user password
     * @return a Map containing the session id associated to the login process in case of success
     */
    public Map login (String user, String pwd)
    {
        return um.login(user, pwd);
    }

    /**
     * Logout from FusionDirectory
     * @param sesionid: the session id previously used for logging in
     */
    public void logout (String sesionid)
    {
        um.logout(sesionid);
    }

    /**
     * Initialises the wrapper instance
     * @param fdUrl The URL of the FusionDirectory server to use
     * @param admin The login name of a FusionDirectory administrator user
     * @param adminPwd The password of the FusionDirectory administrator user
     * @param ldapBaseRDN The LDAP base RDN (e.g. "dc=ow2,dc=org")
     * @param ldapUserRDN The LDAP user RDN (e.g. "ou=people")
     *                     User john will be "uid=john,ou=people,dc=ow2,dc=org"
     */
    public void init(String fdUrl, String admin, String adminPwd, String ldapBaseRDN, String ldapUserRDN) throws Exception {
        um = new UserManagementImpl(fdUrl, admin, adminPwd, ldapBaseRDN, ldapUserRDN);
    }

    /**
     * Returns the e-mail associated to a given user (login name)
     * @param login The user login name
     * @return A Map containing error and errorType, and result with the user e-mail address.
     * If no e-mail address was found for the given login name, the result is null.
     */
    public Map getEmail(String login) {
        return um.getEmail(login);
    }

    /**
     * Gets the first name of a user identified by a login name
     * @param userId : the user login name
     * @return A Map containing error and errorType, and result with the user first name.
     * If no first name was found for the given login name, the result is null.
     */
    public Map getFirstName(String userId) {
        return um.getFirstName(userId);
    }

    /**
     * Gets the last name of a user identified by a login name
     * @param userId: the user login name
     * @return A Map containing error and errorType, and result with the user last name.
     * If no last name was found for the given login name, the result is null.
     */
    public Map getLastName(String userId) {
        return um.getLastName(userId);
    }

    /**
     * Gets the country of a user identified by a login name
     * @param userId: the user login name
     * @return A Map containing error and errorType, and result with the user country.
     * If no country was found for the given login name, the result is null.
     */
    public Map getUserCountry(String userId)  {
        return um.getUserCountry(userId);
    };

    /**
     * Sets the country of a user identified by a login name
     * @param userId: the user login name
     * @param country: the country to be set for this user
     * @return A Map containing error and errorType, and result with the user id.
     */
    public Map setUserCountry(String userId, String country)  {
        return um.setUserCountry(userId, country);
    };

    /**
     * Gets the login name of a user identified by an email address
     * @param email: the user e-mail
     * @return A Map containing error and errorType, and result with the username.
     * If no username was found for the given email, the result is null.
     */
    public Map getUserName(String email) {
        return um.getUserName(email);
    }

    /**
     * Gets the list of members of a group
     * @param group: the full group name,
     *             e.g. "cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org"
     * @return A Map containing error and errorType, and result with an array of usernames.
     */
    public Map getGroupMembers(String group) {
        return um.getGroupMembers(group);
    }

    /**
     * Adds a user to a group
     * @param group: the full group name,
     *             e.g. "cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org"
     * @param member: the member to add to the group
     * @return A Map containing error and errorType, and result with an array of usernames in that group
     */
    public Map addUserToGroup(String group, String member) {
        return um.addUserToGroup(group, member);
    }

    /**
     * Removes a user from a group
     * @param group: the full group name,
     *             e.g. "cn=reachout-limesurvey,ou=groups,ou=reachout,ou=projects,dc=ow2,dc=org"
     * @param member: the member to remove from the group
     * @return A Map containing error and errorType, and result with an array of usernames in that group
     */
    public Map removeUserFromGroup(String group, String member) {
        return um.removeUserFromGroup(group, member);
    }

    /**
     * Get the groups to which a user belongs
     * @param user: the user login name
     * @return A Map containing error and errorType, and result with an array of groups to which the user belongs
     */
    public Map getUserGroups(String user)  {
        return um.getUserGroups(user); }

    /**
     * Creates a new user
     * @param loginname: the user login name
     * @param first: the user first name
     * @param last: the user last name
     * @param email: the user email
     * @param password: the user password
     * @param password2: the user confirmed password
     * @param country: the user country
     * @return A Map containing error and errorType, and result with the user creation result
     */
    public Map createUser(String loginname, String first, String last, String email, String password, String password2, String country) {
        return um.createUser(loginname, first, last, email, password, password2, country);
    }

    /**
     * Deletes a new user
     * @param loginname: the user login name
     * @return A Map containing error and errorType, and result with the user deleting result
     */
    public Map deleteUser(String loginname) {
        return um.deleteUser(loginname);
    }

    /**
     * Resets the password of a user
     * @param loginname: the user login name for which to reset the password
     * @param password: the user new password
     * @param password2: the user confirmed new password
     * @return A Map containing error and errorType, and result with the password reset result
     */
    public Map resetUserPassword(String loginname, String password, String password2) {
        return um.resetUserPassword(loginname, password, password2);
    }

    /**
     * Locks a user in the directory
     * @param loginname: the user login name to lock
     * @return A Map containing error and errorType, and result of locking the user
     */
    public Map lockUser(String loginname) {
        return um.lockUser(loginname);
    }

    /**
     * Unlocks a user in the directory
     * @param loginname: the user login name to unlock
     * @return A Map containing error and errorType, and result of unlocking the user
     */
    public Map unlockUser(String loginname) {
        return um.unlockUser(loginname);
    }

    /**
     * Calls the FusionDirectory "ls" method without having to log in first.
     * Parameters are, according to https://api.fusiondirectory.org/classfdRPCService.html#a8398ebf167739c4b6e654daec180a5dd
     * @param type: the objectType to list
     * @param attrs: The attributes to fetch. If this is a single value, the resulting associative array
     *             will have for each dn the value of this attribute.
     *             If this is an array, the keys must be the wanted attributes,
     *             and the values can be either 1, '*' or 'raw'
     * @param ou: the LDAP branch to search in, base will be used if it is NULL
     * @param filter: an additional filter to use in the LDAP search.
     * @return A Map containing error and errorType, and result.
     */
    public Map ls(String type, Object attrs, String ou, String filter) {
        return um.ls(type, attrs, ou, filter);
    }

    /**
     * Generic call method to the FusionDirectory JSON API without having to login in first.
     * Note that, for several methods, login must have been performed previously and
     * a session id provided as the first parameter.
     * See the FusionDirectory API for more details.
     * https://api.fusiondirectory.org/classfdRPCService.html
     * @param method: the method to call
     * @param params: an array of Objects containing the parameters
     *              Supported type for Objects are String, String[] or Map with String values.
     * @return a JSONObject containing the parsed output
     */
    public JSONObject wrapperCall(String method, Object[] params) {
        return um.wrapperCall(method, params);
    }

    /**
     * Calls the FusionDirectory "getfields" method without having to log in first.
     * Parameters are, according to https://api.fusiondirectory.org/classfdRPCService.html#a38cdd9ec1c53f293dfddfa7dcc263d0d
     * @param type: the object type
     * @param objectDN: the object to load value from
     * @param tab: the tab to return
     * @return A Map containing error and errorType, and result.
     */
    public Map getFields(String type, String objectDN, String tab) {
        return um.getFields(type, objectDN, tab);
    }

    /**
     * From a given userId, groupId and filter, this method:
     * 1. checks whether the user exists
     * 2. whether the user is a member of the input group
     * 3. if the user is NOT a member of the group, get the list of organizations with the filter for
     * which the user is a manager.
     * @param userId: the userId to check
     * @param groupId: the fully qualified DN for the group to which the user should not belong
     * @param orgRDN: the organization DN (e.g. ou=organizations)
     * @param filter: a filter to apply to the organizations
     * @return A Map containing error and errorType, and result.
     * result is a map with 3 entries: userExists (boolean value), isGroupMember (boolean value),
     * orgsManaged (ArrayList with the managed organizations).
     *
     */
    public Map isUserNotInGroupAndOrgManager(String userId, String groupId, String orgRDN, String filter) {
        return um.isUserNotInGroupAndOrgManager(userId, groupId, orgRDN, filter);
    }

    /**
     * Generic call method to the FusionDirectory JSON API.
     * Note that, for several methods, login must have been performed previously and
     * a session id provided as the first parameter.
     * See the FusionDirectory API for more details.
     * https://api.fusiondirectory.org/classfdRPCService.html
     * @param method: the method to call
     * @param params: an array of Objects containing the parameters
     *              Supported type for Objects are String, String[] or Map with String values.
     * @return a JSONObject containing the parsed output
     */
    public JSONObject call(String method, Object[] params) {
        return um.call(method, params);
    }
}
